package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.dtos.response_entities.MessageResponse;
import ro.tuc.ds2020.dtos.response_entities.ResetRequest;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.repositories.PersonRepository;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PersonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PersonService.class);
    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<PersonDTO> findPersons() {
        List<Person> personList = personRepository.findAll();
        return personList.stream()
                .map(PersonBuilder::toUserDTO)
                .collect(Collectors.toList());
    }

    public PersonDTO findPersonById(Long id) {
        Optional<Person> prosumerOptional = personRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Person with id {} was not found in db", id);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with id: " + id);
        }
        return PersonBuilder.toUserDTO(prosumerOptional.get());
    }

    public Person findPersonByEmail(String email) {
        List<Person> foundPersons = personRepository.findByEmail(email);
        if (foundPersons.isEmpty()) {
            LOGGER.error("Person with name {} was not found in db", email);
            throw new ResourceNotFoundException(Person.class.getSimpleName() + " with email: " + email);
        }

        Person foundPerson = foundPersons.get(0);
        return foundPerson;
    }

    public Long insert(PersonDTO personDTO) {
        Person person = PersonBuilder.toEntity(personDTO);
        person = personRepository.save(person);
        LOGGER.debug("Person with id {} was inserted in db", person.getId());
        return person.getId();
    }

    public String hashPassword(String pw)
    {
        byte[] bytesOfPassword=pw.getBytes(StandardCharsets.UTF_8);
        byte[] hashedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            hashedPassword = md.digest(bytesOfPassword);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String newPassword=new String(hashedPassword);
        return newPassword;
    }

    public Person update(ResetRequest credentials) {
        System.out.println(credentials.getEmail());
        List<Person> foundPersons = personRepository.findByEmail(credentials.getEmail());

        if (foundPersons.isEmpty()) {
            LOGGER.error("Person was not found in db");
            throw new ResourceNotFoundException(Person.class.getSimpleName());
        }

        Person foundPerson = foundPersons.get(0);
        String hashedPassword = hashPassword(credentials.getOldPassword());

        if (!hashedPassword.equals(foundPerson.getPassword())){
            return null;
        }

        personRepository.deleteById(foundPerson.getId());
        foundPerson.setPassword(hashPassword(credentials.getNewPassword()));

        foundPerson = personRepository.save(foundPerson);
        return foundPerson;
    }


}
