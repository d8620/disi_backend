package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.dtos.WishDTO;
import ro.tuc.ds2020.dtos.builders.PlaceBuilder;
import ro.tuc.ds2020.dtos.builders.WishBuilder;
import ro.tuc.ds2020.entities.Place;
import ro.tuc.ds2020.entities.Wish;
import ro.tuc.ds2020.repositories.PlaceRepository;
import ro.tuc.ds2020.repositories.WishRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WishService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlaceService.class);
    private final WishRepository wishRepository;
    private final PlaceRepository placeRepository;

    @Autowired
    public WishService(WishRepository wishRepository, PlaceRepository placeRepository) {
        this.wishRepository = wishRepository;
        this.placeRepository = placeRepository;
    }

    public List<WishDTO> findWishes(){
        List<Wish> wishList = wishRepository.findAll();
        return wishList.stream()
                .map(WishBuilder::toWishDTO)
                .collect(Collectors.toList());
    }

    public List<PlaceDTO> findWishesForUser(Long id){
        List<Wish> wishList = wishRepository.findAll();
        List<Wish> wishListToReturn = new ArrayList<>();
        List<Place> places = new ArrayList<>();
        List<PlaceDTO> placesToReturn = new ArrayList<>();

        for (Wish w : wishList){
            if (w.getId_user().equals(id)){
                wishListToReturn.add(w);
            }
        }
        if (wishListToReturn.size() == 0)
            return null;
        else{
            for (Wish w : wishListToReturn){
                Optional<Place> foundPlace = placeRepository.findById(w.getId_place());
                foundPlace.ifPresent(places::add);
            }
            return places.stream()
                    .map(PlaceBuilder::toPlaceDTO)
                    .collect(Collectors.toList());
        }
    }

    public Long insert(WishDTO wishDTO){
        Wish wish = WishBuilder.toEntity(wishDTO);
        wish = wishRepository.save(wish);
        return wish.getId();
    }
}
