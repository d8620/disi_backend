package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.dtos.builders.PlaceBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.entities.Place;
import ro.tuc.ds2020.entities.Wish;
import ro.tuc.ds2020.repositories.PlaceRepository;
import ro.tuc.ds2020.repositories.WishRepository;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class PlaceService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PlaceService.class);
    private final PlaceRepository placeRepository;

    @Autowired
    public PlaceService(PlaceRepository placeRepository) {
        this.placeRepository = placeRepository;
    }

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;


    public List<PlaceDTO> findPlaces() {
        List<Place> placesList = placeRepository.findAll();
        return placesList.stream()
                .map(PlaceBuilder::toPlaceDTO)
                .collect(Collectors.toList());
    }

    public List<Place> findPlacesEntity() {
        List<Place> placesList = placeRepository.findAll();
        return placesList;
    }

    public PlaceDTO findPlaceById(Long id) {
        Optional<Place> prosumerOptional = placeRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Place with id {} was not found in db", id);
            throw new ResourceNotFoundException(Place.class.getSimpleName() + " with id: " + id);
        }
        return PlaceBuilder.toPlaceDTO(prosumerOptional.get());
    }

    public long insert(PlaceDTO placeDTO) {
        Place place = PlaceBuilder.toEntity(placeDTO);
        place = placeRepository.save(place);
        LOGGER.debug("Place with id {} was inserted in db", place.getId());
        return place.getId();
    }

    public void delete(PlaceDTO placeDTO) {
        Place place = PlaceBuilder.toEntity(placeDTO);
        long id = place.getId();
        placeRepository.delete(place);
        LOGGER.debug("Place with id {} was deleted in db", id);
    }

    public void deleteById(Long id) {
        if (id == null) {
            return;
        }
        placeRepository.deleteById(id);
    }

    public PlaceDTO update(Long id, PlaceDTO placeDTO) {
        Optional<Place> placeToUpdate = placeRepository.findById(id);

        if (!placeToUpdate.isPresent()) {
            LOGGER.error("Client with id {} was not found in db", id);
            throw new ResourceNotFoundException(Place.class.getSimpleName() + " with id: " + id);
        }

        Place place = PlaceBuilder.toEntity(placeDTO);

        int nr_of_tourist = placeDTO.getNr_of_tourists();
        if (nr_of_tourist > 100 && nr_of_tourist < 200)
            placeDTO.setDiscount(20);
        else if (nr_of_tourist > 199 && nr_of_tourist < 500)
            placeDTO.setDiscount(50);


        placeRepository.update(id, placeDTO.getName(), placeDTO.getLocation(), placeDTO.getDescriptionText(), placeDTO.getDescriptionAudio(), placeDTO.getPrice(), placeDTO.getDiscount(), placeDTO.getNr_of_tourists(), placeDTO.getCategory(), placeDTO.getImage());
//        place.setName(placeDTO.getName());
//        place.setLocation(placeDTO.getLocation());
//        place.setDescriptionText(placeDTO.getDescriptionText());
//        place.setDescriptionAudio(placeDTO.getDescriptionAudio());
//        place.setPrice(placeDTO.getPrice());
//        place.setNr_of_tourists(placeDTO.getNr_of_tourists());
//        place.setCategory(placeDTO.getCategory());
//        place.setImage(placeDTO.getImage());
//
//        place = placeRepository.save(place);

        LOGGER.debug("Place with id {} was updated in db", place.getId());
        return PlaceBuilder.toPlaceDTO(place);
    }

    public List<Place> getSearchByLocation(String location) {
        if (location != null) {
            return placeRepository.searchLocation(location);
        }
        return findPlacesEntity();
    }

    public List<Place> getSearchByCategory(String category) {
        if (category != null) {
            return placeRepository.searchCategory(category);
        }
        return findPlacesEntity();
    }

    private void sendNotification (Long id, WishRepository wishRepository) {
        List<Wish> listWish = wishRepository.findAll();
        for (Wish w: listWish) {
            if (w.getId_place() == id) {
                Long id_user = w.getId_user();
                Optional<Place> place = placeRepository.findById(id);
                simpMessagingTemplate.convertAndSend("/topic/socket/place/values",
                        "A aparut o oferta pentru obiectivul " + place.toString());
            }
        }
    }


}
