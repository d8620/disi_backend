package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.CommentDTO;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.dtos.builders.CommentBuilder;
import ro.tuc.ds2020.dtos.builders.PlaceBuilder;
import ro.tuc.ds2020.entities.Comment;
import ro.tuc.ds2020.entities.Place;
import ro.tuc.ds2020.repositories.CommentRepository;
import ro.tuc.ds2020.repositories.PlaceRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CommentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentService.class);
    private final CommentRepository commentRepository;
    private final PlaceRepository placeRepository;

    @Autowired
    public CommentService(CommentRepository commentRepository, PlaceRepository placeRepository) {
        this.commentRepository = commentRepository;
        this.placeRepository = placeRepository;
    }

    public List<CommentDTO> findComments() {
        List<Comment> commentsList = commentRepository.findAll();
        return commentsList.stream()
                .map(CommentBuilder::toCommentDTO)
                .collect(Collectors.toList());
    }
    public CommentDTO findCommentById(Long id) {
        Optional<Comment> prosumerOptional = commentRepository.findById(id);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("Comment with id {} was not found in db", id);
            throw new ResourceNotFoundException(Place.class.getSimpleName() + " with id: " + id);
        }
        return CommentBuilder.toCommentDTO(prosumerOptional.get());
    }

    public Long insert(CommentDTO commentDTO) {
        Comment comment = CommentBuilder.toEntity(commentDTO);
        comment = commentRepository.save(comment);
        LOGGER.debug("Comment with id {} was inserted in db", comment.getId());
        return comment.getId();
    }
    public List<CommentDTO> findCommentsForUser(Long id){
        List<Comment> commentList = commentRepository.findAll();
        List<Comment> commentListToReturn = new ArrayList<>();
        List<Comment> comments = new ArrayList<>();
        List<CommentDTO> commentsToReturn = new ArrayList<>();

        for (Comment w : commentList){
            if (w.getId_place().equals(id)){
                commentListToReturn.add(w);
            }
        }

        return commentListToReturn.stream()
                .map(CommentBuilder::toCommentDTO)
                .collect(Collectors.toList());

    }


    public void delete(CommentDTO commentDTO) {
        Comment comment = CommentBuilder.toEntity(commentDTO);
        Long id = comment.getId();
        commentRepository.delete(comment);
        LOGGER.debug("Comment with id {} was deleted in db", id);
    }

    public void deleteById(Long id) {
        if (id == null) {
            return;
        }
        commentRepository.deleteById(id);
    }

    public CommentDTO update(Long id, CommentDTO commentDTO) {
        CommentDTO commentToDelete = findCommentById(id);
        System.out.println(commentToDelete.getId());
        System.out.println(commentToDelete.getId_place());
        commentRepository.deleteById(commentToDelete.getId());

        Comment comment = CommentBuilder.toEntity(commentDTO);
        comment = commentRepository.save(comment);
        LOGGER.debug("Comment with id {} was updated in db", comment.getId());
        return CommentBuilder.toCommentDTO(comment);
    }



}
