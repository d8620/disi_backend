from flask import Flask, request
from flask_mail import Mail, Message
import base64
from flask_cors import CORS, cross_origin

app = Flask(__name__)
mail= Mail(app)
CORS(app, support_credentials=True)

app.debug = True

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'andreeaivascu123@gmail.com'
app.config['MAIL_PASSWORD'] = "TestforPIR123."
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)


@app.route("/contact/", methods=['POST'])
@cross_origin(supports_credentials=True)
def index():
   data = request.get_json(force=True)
   print(data)
   msg = Message('Travel', sender = data['sender'], recipients = ['touristic.objectives.agency@gmail.com'])
   msg.body = data['body']
   mail.send(msg)
   return msg.body

if __name__ == '__main__':
   app.run()
