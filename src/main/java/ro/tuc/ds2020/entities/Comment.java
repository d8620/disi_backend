package ro.tuc.ds2020.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "id_place", nullable = false)
    private Long id_place;

    @Column(name = "description", nullable = false)
    private String description;

    @Column(name = "grade")
    private int grade;

    @Column(name = "user_email")
    private String user_email;


    public Comment(Long id_place, String description, int grade, String user_email) {
        this.id_place = id_place;
        this.description = description;
        this.grade = grade;
        this.user_email = user_email;
    }
}
