package ro.tuc.ds2020.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Place implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "location")
    private String location;

    @Column(name = "descriptionText")
    private String descriptionText;

    @Column(name = "descriptionAudio")
    private String descriptionAudio;

    @Column(name = "price")
    private int price;

    @Column(name = "discount")
    private int discount;

    @Column(name = "nr_of_tourists")
    private int nr_of_tourists;

    @Column(name = "category")
    private String category;

    @Column(name = "id_comments")
    private Long id_comments;

    @Column(name = "image")
    private String image;

    public Place(String name, String location, String descriptionText, String descriptionAudio, int price, int discount, int nr_of_tourists, String category, Long id_comments, String image) {
        this.name = name;
        this.location = location;
        this.descriptionText = descriptionText;
        this.descriptionAudio = descriptionAudio;
        this.price = price;
        this.discount = discount;
        this.nr_of_tourists = nr_of_tourists;
        this.category = category;
        this.id_comments = id_comments;
        this.image = image;
    }
}
