package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class CommentDTO extends RepresentationModel<CommentDTO> {

    private Long id;
    private Long id_place;
    private String description;
    private int grade;
    private String user_email;

    public CommentDTO(Long id_place, String description, int grade, String user_email) {
        this.id_place = id_place;
        this.description = description;
        this.grade = grade;
        this.user_email = user_email;
    }
}
