package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WishDTO extends RepresentationModel<WishDTO> {

    private Long id;
    private Long id_user;
    private Long id_place;

    public WishDTO(Long id_user, Long id_place) {
        this.id_user = id_user;
        this.id_place = id_place;
    }
}
