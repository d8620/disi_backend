package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.entities.Person;

@NoArgsConstructor
public class PersonBuilder {

    public static PersonDTO toUserDTO(Person person) {
        return new PersonDTO(person.getId(), person.getName(), person.getPassword(),
                person.getEmail(),
                person.getRole(),
                person.getId_places());
    }


    public static Person toEntity(PersonDTO personDTO) {
        return new Person(personDTO.getName(),
                personDTO.getPassword(),
                personDTO.getEmail(),
                personDTO.getRole(),
                personDTO.getId_places());
    }
}
