package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.CommentDTO;
import ro.tuc.ds2020.entities.Comment;

@NoArgsConstructor
public class CommentBuilder {

    public static CommentDTO toCommentDTO(Comment comment) {
        return new CommentDTO(comment.getId(),
                comment.getId_place(),
                comment.getDescription(),
                comment.getGrade(),
                comment.getUser_email());
    }

    public static Comment toEntity(CommentDTO commentDTO) {
        return new Comment(commentDTO.getId_place(),
                commentDTO.getDescription(),
                commentDTO.getGrade(),
                commentDTO.getUser_email());
    }

}
