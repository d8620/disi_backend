package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.entities.Place;

@NoArgsConstructor
public class PlaceBuilder {

    public static PlaceDTO toPlaceDTO(Place place) {
        return new PlaceDTO(place.getId(),
                place.getName(),
                place.getLocation(),
                place.getDescriptionText(),
                place.getDescriptionAudio(),
                place.getPrice(),
                place.getDiscount(),
                place.getNr_of_tourists(),
                place.getCategory(),
                place.getId_comments(),
                place.getImage());
    }

    public static Place toEntity(PlaceDTO placeDTO) {
        return new Place(placeDTO.getId(),
                placeDTO.getName(),
                placeDTO.getLocation(),
                placeDTO.getDescriptionText(),
                placeDTO.getDescriptionAudio(),
                placeDTO.getPrice(),
                placeDTO.getDiscount(),
                placeDTO.getNr_of_tourists(),
                placeDTO.getCategory(),
                placeDTO.getId_comments(),
                placeDTO.getImage());
    }

}
