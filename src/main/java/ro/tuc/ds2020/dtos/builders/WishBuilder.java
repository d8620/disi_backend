package ro.tuc.ds2020.dtos.builders;

import lombok.NoArgsConstructor;
import ro.tuc.ds2020.dtos.WishDTO;
import ro.tuc.ds2020.entities.Wish;

@NoArgsConstructor
public class WishBuilder {

    public static WishDTO toWishDTO(Wish wish){
        return new WishDTO(wish.getId(),
                wish.getId_user(),
                wish.getId_place());
    }

    public static Wish toEntity(WishDTO wishDTO){
        return new Wish(wishDTO.getId_user(),
                wishDTO.getId_place());
    }
}
