package ro.tuc.ds2020.dtos.response_entities;

import javax.persistence.Column;
import java.util.UUID;

public class LoginResponse {

    private Long id;
    private String role;

    public LoginResponse(Long id, String role) {
        this.id = id;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
