package ro.tuc.ds2020.dtos.response_entities;

public class ReviewForm {

    private String description;
    private int grade;
    private String user_email;

    public ReviewForm(String description, int grade, String user_email) {
        this.description = description;
        this.grade = grade;
        this.user_email = user_email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }
}
