package ro.tuc.ds2020.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import java.util.Objects;
import java.util.UUID;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PersonDTO extends RepresentationModel<PersonDTO> {

    private Long id;
    private String name;
    private String password;
    private String email;
    private String role;
    private Long id_places;

    public PersonDTO(String name, String password, String email, String role, Long id_places) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.role = role;
        this.id_places = id_places;
    }
}
