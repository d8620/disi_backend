package ro.tuc.ds2020.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.RepresentationModel;

import javax.persistence.Column;
import java.util.UUID;

@NoArgsConstructor
@Data
public class PlaceDTO extends RepresentationModel<PlaceDTO> {

    private Long id;
    private String name;
    private String location;
    private String descriptionText;
    private String descriptionAudio;
    private int price;
    private int discount;
    private int nr_of_tourists;
    private String category;
    private Long id_comments;
    private String image;

    public PlaceDTO(Long id, String name, String location, String descriptionText, String descriptionAudio, int price, int discount, int nr_of_tourists, String category, Long id_comments, String image) {
        this.id = id;
        this.name = name;
        this.location = location;
        this.descriptionText = descriptionText;
        this.descriptionAudio = descriptionAudio;
        this.price = price;
        this.discount = discount;
        this.nr_of_tourists = nr_of_tourists;
        this.category = category;
        this.id_comments = id_comments;
        this.image = image;
    }
}
