package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CommentDTO;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.dtos.response_entities.MessageResponse;
import ro.tuc.ds2020.dtos.response_entities.ReviewForm;
import ro.tuc.ds2020.entities.Place;
import ro.tuc.ds2020.services.CommentService;
import ro.tuc.ds2020.services.PlaceService;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/details-user")
public class CommentController {

    private final CommentService commentService;
    private final PlaceService placeService;

    @Autowired

    public CommentController(CommentService commentService, PlaceService placeService) {
        this.commentService = commentService;
        this.placeService = placeService;
    }

    @GetMapping()
    public ResponseEntity<List<CommentDTO>> getComments() {
        List<CommentDTO> dtos = commentService.findComments();
        for (CommentDTO dto : dtos) {
            Link placeLink = linkTo(methodOn(CommentController.class)
                    .getComment(dto.getId())).withRel("commentDetails");
            dto.add(placeLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

   /* @GetMapping("/comments")
    public ResponseEntity<List<CommentDTO>> getCommentsById(Long placeId) {
        List<CommentDTO> dtos = commentService.findComments();
        for (CommentDTO dto : dtos) {
            Link placeLink = linkTo(methodOn(CommentController.class)
                    .getComment(dto.getId())).withRel("commentDetails");
            if (dto.getId_place() == placeId)
                dto.add(placeLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }*/

    @GetMapping("/comments/{id}")
    public ResponseEntity<?> getCommentsForUser(@PathVariable Long id){
        List<CommentDTO> dtos = commentService.findCommentsForUser(id);
        if (dtos != null)
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        else
            return ResponseEntity.badRequest().body(new MessageResponse("Commentslist not found"));
    }


    @PostMapping()
    public ResponseEntity<Long> insertComment(@Valid @RequestBody CommentDTO commentDTO) {
        Long commentId = commentService.insert(commentDTO);
        return new ResponseEntity<>(commentId, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteCommentById(@PathVariable Long id) {
        commentService.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<CommentDTO> updateComment(@PathVariable("id") Long commentId, @RequestBody CommentDTO dto) {
        return ResponseEntity.status(HttpStatus.OK).body(commentService.update(commentId,dto));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CommentDTO> getComment(@PathVariable("id") Long commentId) {
        CommentDTO dto = commentService.findCommentById(commentId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping("/reviews/{idPlace}")
    public ResponseEntity<Long> addReview(@PathVariable("idPlace") Long idPlace, @RequestBody ReviewForm reviewForm) {
        Long commentId = commentService.insert(new CommentDTO(idPlace, reviewForm.getDescription(), reviewForm.getGrade(), reviewForm.getUser_email()));

        PlaceDTO placeDTO = placeService.findPlaceById(idPlace);
        placeDTO.setId_comments(commentId);
        System.out.println(placeDTO.getId_comments());
        //placeService.update(idPlace,placeDTO); ////NUUUUUUUUUUUUUUU

        return new ResponseEntity<>(commentId, HttpStatus.CREATED);
    }


}
