package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.dtos.response_entities.MessageResponse;
import ro.tuc.ds2020.entities.Place;
import ro.tuc.ds2020.services.PlaceService;

import java.util.*;

import org.springframework.hateoas.Link;

import javax.validation.Valid;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;


@RestController
@CrossOrigin("*")
@RequestMapping(value = "/place")
public class PlaceController {

    private final PlaceService placeService;

    @Autowired

    public PlaceController(PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping()
    public ResponseEntity<List<PlaceDTO>> getPlaces() {
        List<PlaceDTO> dtos = placeService.findPlaces();
        for (PlaceDTO dto : dtos) {
            Link placeLink = linkTo(methodOn(PlaceController.class)
                    .getPlace(dto.getId())).withRel("placeDetails");
            dto.add(placeLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<Long> insertPlace(@Valid @RequestBody PlaceDTO placeDTO) {
        long placeID = placeService.insert(placeDTO);
        return new ResponseEntity<>(placeID, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deletePlaceById(@PathVariable Long id) {
        placeService.deleteById(id);
        return ResponseEntity.ok(new MessageResponse("Deleted!"));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PlaceDTO> updatePlace(@PathVariable("id") Long placeId, @RequestBody PlaceDTO dto) {
        return ResponseEntity.status(HttpStatus.OK).body(placeService.update(placeId,dto));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PlaceDTO> getPlace(@PathVariable("id") Long placeId) {
        PlaceDTO dto = placeService.findPlaceById(placeId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @GetMapping("/sortName/{location}")
    public ResponseEntity<List<Place>> getSearchByLocation(@PathVariable("location") String location) {
        List<Place> dtos = placeService.getSearchByLocation(location);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/sortCategory/{category}")
    public ResponseEntity<List<Place>> getSearchByCategory(@PathVariable("category") String category) {
        List<Place> dtos = placeService.getSearchByCategory(category);
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/locationsGraphic")
    public ResponseEntity<Map<Long, Place>> getLocationsGraphic() {
        List<PlaceDTO> dtos = placeService.findPlaces();
        Map map = new HashMap();

        for (PlaceDTO dto : dtos) {
            Link placeLink = linkTo(methodOn(PlaceController.class)
                    .getPlace(dto.getId())).withRel("placeDetails");
            dto.add(placeLink);
            map.put(dto.getNr_of_tourists(), dto.getLocation());
        }
        Set set=map.entrySet();//Converting to Set so that we can traverse
        Iterator itr=set.iterator();

        while(itr.hasNext()){
            Map.Entry entry=(Map.Entry)itr.next();
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @GetMapping("/objectivesGraphic")
    public ResponseEntity<Map<Long, Place>> getObjectivesGraphic() {
        List<PlaceDTO> dtos = placeService.findPlaces();
        Map map = new HashMap();

        for (PlaceDTO dto : dtos) {
            Link placeLink = linkTo(methodOn(PlaceController.class)
                    .getPlace(dto.getId())).withRel("placeDetails");
            dto.add(placeLink);
            map.put(dto.getNr_of_tourists(), dto.getName());
        }
        Set set=map.entrySet();
        Iterator itr=set.iterator();

        while(itr.hasNext()){
            Map.Entry entry=(Map.Entry)itr.next();
            System.out.println(entry.getKey()+" "+entry.getValue());
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

}
