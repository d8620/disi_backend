package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.response_entities.MessageResponse;
import ro.tuc.ds2020.dtos.response_entities.ResetRequest;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.PersonService;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping()
    public ResponseEntity<List<PersonDTO>> getPersons() {
        List<PersonDTO> dtos = personService.findPersons();
//        for (PersonDTO dto : dtos) {
//            Link personLink = linkTo(methodOn(PersonController.class)
//                    .getPerson(dto.getId())).withRel("personDetails");
//            dto.add(personLink);
//        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }


    @GetMapping(value = "/get-by-email/{email}")
    public ResponseEntity<Person> getPerson(@PathVariable("email") String email) {
        Person person = personService.findPersonByEmail(email);
        //PersonDTO dto = personService.findPersonById(personId);
        return new ResponseEntity<>(person, HttpStatus.OK);
    }

    @PutMapping(value = "/reset-password")
    public ResponseEntity<?> updatePerson(@RequestBody ResetRequest credentials) {
        if (personService.update(credentials) == null)
            return ResponseEntity.badRequest().body(new MessageResponse("Wrong credentials"));
        else
            return ResponseEntity.ok(HttpStatus.OK);
    }

}
