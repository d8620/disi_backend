package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.response_entities.LoginRequest;
import ro.tuc.ds2020.dtos.response_entities.LoginResponse;
import ro.tuc.ds2020.dtos.response_entities.MessageResponse;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.response_entities.RegisterRequest;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/person")
public class LoginController {

    private final PersonService personService;

    @Autowired
    public LoginController(PersonService personService) {
        this.personService = personService;
    }

    @PostMapping("/login")
    @ResponseBody
    public ResponseEntity<?> processLoginForm(@RequestBody LoginRequest loginRequest) {

        Person person = personService.findPersonByEmail(loginRequest.getEmail());

        String password= loginRequest.getPassword();
        String hashedPassword = hashPassword(password);

        if (person==null || !person.getPassword().equals(hashedPassword))
            return ResponseEntity.badRequest().body(new MessageResponse("Wrong credentials"));
        else
            return ResponseEntity.ok(new LoginResponse(person.getId(), person.getRole()));

    }

    @PostMapping("/register")
    public ResponseEntity<Long> processRegisterForm(@Valid @RequestBody RegisterRequest registerRequest) {
        PersonDTO personDTO = new PersonDTO();
        personDTO.setName(registerRequest.getUsername());
        personDTO.setEmail(registerRequest.getEmail());
        personDTO.setRole("user");
        personDTO.setId_places(null);
        String hashedPassword = hashPassword(registerRequest.getPassword());
        personDTO.setPassword(hashedPassword);
        Long personID = personService.insert(personDTO);
        return new ResponseEntity<>(personID, HttpStatus.CREATED);
    }

    public String hashPassword(String pw)
    {
        byte[] bytesOfPassword=pw.getBytes(StandardCharsets.UTF_8);
        byte[] hashedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            hashedPassword = md.digest(bytesOfPassword);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        String newPassword=new String(hashedPassword);
        return newPassword;
    }

}
