package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.dtos.WishDTO;
import ro.tuc.ds2020.dtos.response_entities.MessageResponse;
import ro.tuc.ds2020.services.WishService;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping(value = "/user")
public class WishController {

    private final WishService wishService;

    @Autowired
    public WishController(WishService wishService) {
        this.wishService = wishService;
    }

    @GetMapping()
    public ResponseEntity<List<WishDTO>> getWishes(){
        List<WishDTO> dtos = wishService.findWishes();
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getWishesForUser(@PathVariable Long id){
        List<PlaceDTO> dtos = wishService.findWishesForUser(id);
        if (dtos != null)
            return new ResponseEntity<>(dtos, HttpStatus.OK);
        else
            return ResponseEntity.badRequest().body(new MessageResponse("Wishlist not found"));
    }

    @PostMapping()
    public ResponseEntity<Long> insertWish(@Valid @RequestBody WishDTO wishDTO) {
        long wishID = wishService.insert(wishDTO);
        return new ResponseEntity<>(wishID, HttpStatus.CREATED);
    }
}
