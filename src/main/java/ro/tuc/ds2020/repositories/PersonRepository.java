package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Person;

import java.util.List;
import java.util.UUID;

public interface PersonRepository extends JpaRepository<Person, Long> {

    /**
     * Example: JPA generate Query by Field
     */
    List<Person> findByEmail(String email);

    /**
     * Example: Write Custom Query
     */
//    @Query(value = "SELECT p " +
//            "FROM User p " +
//            "WHERE p.name = :name " +
//            "AND p.age >= 60  ")
//    Optional<User> findSeniorsByName(@Param("name") String name);

}
