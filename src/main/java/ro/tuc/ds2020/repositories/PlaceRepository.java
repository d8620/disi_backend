package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.dtos.PlaceDTO;
import ro.tuc.ds2020.entities.Place;

import java.util.List;
import java.util.Optional;

public interface PlaceRepository extends JpaRepository<Place, Long> {
    @Query("SELECT p FROM Place p WHERE p.location LIKE %?1%")
    List<Place> searchLocation(String location);

    @Query("SELECT p FROM Place p WHERE p.category LIKE %?1%")
    List<Place> searchCategory(String category);

    @Modifying
    @Transactional
    @Query(value = "update Place p set p.name = :name, p.location = :location, p.descriptionText = :descriptionText, p.descriptionAudio = :descriptionAudio, p.price = :price, p.discount = :discount, p.nr_of_tourists = :nr_of_tourists, p.category = :category, p.image = :image WHERE p.id = :id")
    void update(@Param("id") Long id,
                @Param("name") String name,
                @Param("location") String location,
                @Param("descriptionText") String descriptionText,
                @Param("descriptionAudio") String descriptionAudio,
                @Param("price") int price,
                @Param("discount") int discount,
                @Param("nr_of_tourists") int nr_of_tourists,
                @Param("category") String category,
                @Param("image") String image);
}
