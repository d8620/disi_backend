package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Comment;

import java.util.UUID;

public interface CommentRepository extends JpaRepository<Comment, Long> {
}
