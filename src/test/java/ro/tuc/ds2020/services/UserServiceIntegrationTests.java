package ro.tuc.ds2020.services;

import org.springframework.test.context.jdbc.Sql;
import ro.tuc.ds2020.Ds2020TestConfig;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:/test-sql/create.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:/test-sql/delete.sql")
public class UserServiceIntegrationTests extends Ds2020TestConfig {

//    @Autowired
//    PersonService personService;
//
//    @Test
//    public void testGetCorrect() {
//        List<UserDTO> userDTOList = personService.findPersons();
//        assertEquals("Test Insert Person", 1, userDTOList.size());
//    }
//
//    @Test
//    public void testInsertCorrectWithGetById() {
//        UserDetailsDTO p = new UserDTO("John", "Somewhere Else street", 22);
//        UUID insertedID = personService.insert(p);
//
//        UserDetailsDTO insertedPerson = new UserDetailsDTO(insertedID, p.getName(),p.getAddress(), p.getAge());
//        UserDetailsDTO fetchedPerson = personService.findPersonById(insertedID);
//
//        assertEquals("Test Inserted Person", insertedPerson, fetchedPerson);
//    }
//
//    @Test
//    public void testInsertCorrectWithGetAll() {
//        UserDetailsDTO p = new UserDetailsDTO("John", "Somewhere Else street", 22);
//        personService.insert(p);
//
//        List<UserDTO> userDTOList = personService.findPersons();
//        assertEquals("Test Inserted Persons", 2, userDTOList.size());
//    }
}
